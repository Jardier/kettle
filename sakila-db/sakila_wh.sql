
CREATE TABLE dim_customer (
                customer_seq NUMERIC(11) DEFAULT NULL::numeric,
                customer_version NUMERIC(11) DEFAULT NULL::numeric,
                date_from date NULL DEFAULT NULL,
                to_date date NULL DEFAULT NULL,
                customer_id NUMERIC(11) DEFAULT NULL::numeric,
                store_id NUMERIC(11) DEFAULT NULL::numeric,
                first_name VARCHAR(50) DEFAULT NULL::character varying,
                last_name VARCHAR(50) DEFAULT NULL::character varying,
                email VARCHAR(50) DEFAULT NULL::character varying,
                active NUMERIC(11) DEFAULT NULL::numeric,
                address_id NUMERIC(11) DEFAULT NULL::numeric,
                address VARCHAR(50) DEFAULT NULL::character varying,
                city_id NUMERIC(11) DEFAULT NULL::numeric,
                city VARCHAR(50) DEFAULT NULL::character varying,
                country_id NUMERIC(11) DEFAULT NULL::numeric,
                country VARCHAR(50) DEFAULT NULL::character varying,
                active_desc VARCHAR(15) DEFAULT NULL::character varying,
                create_date date NULL DEFAULT NULL,
                last_update date NULL DEFAULT NULL
);


CREATE TABLE dim_store (
                store_id NUMERIC(11) DEFAULT NULL::numeric,
                manager_staff_id NUMERIC(11) DEFAULT NULL::numeric,
                staff_first_name VARCHAR(30) DEFAULT NULL::character varying,
                staff_last_name VARCHAR(30) DEFAULT NULL::character varying,
                address_id NUMERIC(11) DEFAULT NULL::numeric,
                address VARCHAR(50) DEFAULT NULL::character varying,
                city_id NUMERIC(11) DEFAULT NULL::numeric,
                city VARCHAR(50) DEFAULT NULL::character varying,
                country_id NUMERIC(11) DEFAULT NULL::numeric,
                country VARCHAR(50) DEFAULT NULL::character varying,
                last_update TIMESTAMP
);


CREATE TABLE dim_film (
                film_id NUMERIC(11) DEFAULT NULL::numeric,
                title VARCHAR(50) DEFAULT NULL::character varying,
                description VARCHAR(200) DEFAULT NULL::character varying,
                release_year VARCHAR(4) DEFAULT NULL::character varying,
                language_id NUMERIC(11) DEFAULT NULL::numeric,
                lang_name VARCHAR(20) DEFAULT NULL::character varying,
                original_language_id NUMERIC(11) DEFAULT NULL::numeric,
                rental_duration NUMERIC(11) DEFAULT NULL::numeric,
                rental_rate NUMERIC(131089),
                length NUMERIC(11) DEFAULT NULL::numeric,
                length_desc VARCHAR(255) DEFAULT NULL::character varying,
                replacement_cost NUMERIC(131089),
                rating VARCHAR(5) DEFAULT NULL::character varying,
                category_id NUMERIC(11) DEFAULT NULL::numeric,
                category_desc VARCHAR(25) DEFAULT NULL::character varying,
                special_features VARCHAR(100) DEFAULT NULL::character varying,
                has_trailers VARCHAR(3) DEFAULT NULL::character varying,
                has_commentaries VARCHAR(3) DEFAULT NULL::character varying,
                has_behind_the_scenes VARCHAR(3) DEFAULT NULL::character varying,
                has_deleted_scenes VARCHAR(3) DEFAULT NULL::character varying,
                las_update TIMESTAMP
);


CREATE INDEX film_index
 ON dim_film USING BTREE
 ( film_id );

CREATE TABLE dim_staff (
                staff_id NUMERIC(2) DEFAULT NULL::numeric,
                staff_first_name VARCHAR(30) DEFAULT NULL::character varying,
                staff_last_name VARCHAR(30) DEFAULT NULL::character varying,
                email VARCHAR(30) DEFAULT NULL::character varying,
                active NUMERIC(1) DEFAULT NULL::numeric,
                last_update TIMESTAMP,
                active_desc VARCHAR(20) DEFAULT NULL::character varying
);


CREATE TABLE dim_date (
                date_key NUMERIC(11) DEFAULT NULL::numeric NOT NULL,
                datein TIMESTAMP,
                years VARCHAR(4) DEFAULT NULL::character varying,
                days VARCHAR(2) DEFAULT NULL::character varying,
                months VARCHAR(2) DEFAULT NULL::character varying,
                CONSTRAINT dim_date_pkey PRIMARY KEY (date_key)
);


CREATE TABLE fact_rental (
                rental_id NUMERIC(11) DEFAULT NULL::numeric,
                rental_date TIMESTAMP,
                date_key NUMERIC(10) DEFAULT NULL::numeric,
                time_seq NUMERIC(11) DEFAULT NULL::numeric,
                inventory_id NUMERIC(11) DEFAULT NULL::numeric,
                return_date TIMESTAMP,
                last_update TIMESTAMP,
                payment_id NUMERIC(11) DEFAULT NULL::numeric,
                amount NUMERIC(131089),
                payment_date TIMESTAMP,
                payment_last_update TIMESTAMP,
                film_id VARCHAR(4) DEFAULT NULL::character varying,
                store_id NUMERIC(11) DEFAULT NULL::numeric,
                staff_id_rental NUMERIC(11) DEFAULT NULL::numeric,
                staff_id_payment NUMERIC(11) DEFAULT NULL::numeric,
                customer_seq NUMERIC(11) DEFAULT NULL::numeric,
                counter NUMERIC(11) DEFAULT NULL::numeric,
                rental_hours NUMERIC(131089),
                is_return NUMERIC(11) DEFAULT NULL::numeric
);


CREATE INDEX idx_fact_rental_lookup
 ON fact_rental USING BTREE
 ( rental_id );

CREATE TABLE dim_time (
                time_seq NUMERIC(11) DEFAULT NULL::numeric,
                time_desc VARCHAR(5) DEFAULT NULL::character varying,
                hours_id NUMERIC(2) DEFAULT NULL::numeric,
                minutes_id NUMERIC(2) DEFAULT NULL::numeric,
                hours_type VARCHAR(2) DEFAULT NULL::character varying
);
